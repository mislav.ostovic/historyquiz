package com.mislavostovic.historyquiz

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query

class HighscoreActivity : AppCompatActivity() {

    private lateinit var highscoresTextView: TextView
    private lateinit var toMainPage: Button
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_highscore)

        highscoresTextView = findViewById(R.id.highscores_text_view)
        toMainPage = findViewById(R.id.to_main_page_button)

        loadHighscores()

        toMainPage.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }

    private fun loadHighscores() {
        val db = FirebaseFirestore.getInstance()
        db.collection("highscores")
            .orderBy("points", Query.Direction.DESCENDING)
            .limit(10)
            .get()
            .addOnSuccessListener { result ->
                val highscores = StringBuilder()
                for (document in result) {
                    val userId = document.getString("userId")
                    val points = document.getLong("points")
                    highscores.append("User: $userId, Points: $points\n")
                }
                highscoresTextView.text = highscores.toString()
            }
            .addOnFailureListener { exception ->
                highscoresTextView.text = "Error loading highscores: ${exception.message}"
            }
    }
}
