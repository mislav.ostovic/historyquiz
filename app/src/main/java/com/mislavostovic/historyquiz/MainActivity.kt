package com.mislavostovic.historyquiz

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth

class MainActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private lateinit var loginButton: Button
    private lateinit var registerButton: Button
    private lateinit var logoutButton: Button
    private lateinit var playButton: Button
    private lateinit var highscoresButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        loginButton = findViewById(R.id.login_button)
        registerButton = findViewById(R.id.register_button)
        logoutButton = findViewById(R.id.logout_button)
        playButton = findViewById(R.id.play_button)
        highscoresButton = findViewById(R.id.highscores_button)

        auth = FirebaseAuth.getInstance()

        playButton.setOnClickListener {
            if (auth.currentUser != null) {
                val intent = Intent(this, QuizActivity::class.java)
                startActivity(intent)
            } else {
                Toast.makeText(this, "You must login to play", Toast.LENGTH_SHORT).show()
            }
        }

        loginButton.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }

        registerButton.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }

        logoutButton.setOnClickListener {
            auth.signOut()
            logoutButton.visibility = android.view.View.GONE
            loginButton.visibility = android.view.View.VISIBLE
            registerButton.visibility = android.view.View.VISIBLE
        }

        highscoresButton.setOnClickListener {
            val intent = Intent(this, HighscoreActivity::class.java)
            startActivity(intent)
        }


        if (auth.currentUser != null) {
            logoutButton.visibility = android.view.View.VISIBLE
            loginButton.visibility = android.view.View.GONE
            registerButton.visibility = android.view.View.GONE
        }
    }

}
