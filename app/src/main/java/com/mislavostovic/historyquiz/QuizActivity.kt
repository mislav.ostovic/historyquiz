package com.mislavostovic.historyquiz

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class QuizActivity : AppCompatActivity() {

    private val questions = mutableListOf<Question>()
    private var currentQuestionIndex = 0
    private var correctAnswersCount = 0

    private lateinit var nextButton: Button
    private lateinit var questionText: TextView
    private lateinit var answer1: RadioButton
    private lateinit var answer2: RadioButton
    private lateinit var answer3: RadioButton
    private lateinit var answer4: RadioButton
    private lateinit var answerGroup: RadioGroup
    private var totalPoints = 0
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quiz)

        auth = FirebaseAuth.getInstance()

        nextButton = findViewById(R.id.next_button)
        questionText = findViewById(R.id.question_text)
        answer1 = findViewById(R.id.answer1)
        answer2 = findViewById(R.id.answer2)
        answer3 = findViewById(R.id.answer3)
        answer4 = findViewById(R.id.answer4)
        answerGroup = findViewById(R.id.answers_group)

        loadQuestions()

        nextButton.setOnClickListener {
            checkAnswer()
            if (currentQuestionIndex < questions.size - 1) {
                currentQuestionIndex++
                showQuestion(questions[currentQuestionIndex])
            } else {
                saveUserScore(totalPoints)
                showResult(correctAnswersCount, questions.size)
            }
        }
    }

    private fun loadQuestions() {
        val db = FirebaseFirestore.getInstance()
        db.collection("questions")
            .limit(10)
            .get()
            .addOnSuccessListener { result ->
                for (document in result) {
                    val question = document.toObject(Question::class.java)
                    questions.add(question)
                }
                if (questions.isNotEmpty()) {
                    questions.shuffle()
                    showQuestion(questions[0])
                }
            }
            .addOnFailureListener { exception ->
                Toast.makeText(this, "Error loading questions: ${exception.message}", Toast.LENGTH_LONG).show()
            }
    }

    private fun showQuestion(question: Question) {
        questionText.text = question.text
        answer1.text = question.answers[0]
        answer2.text = question.answers[1]
        answer3.text = question.answers[2]
        answer4.text = question.answers[3]
        answerGroup.clearCheck()
    }

    private fun checkAnswer() {
        val selectedAnswerIndex = when (answerGroup.checkedRadioButtonId) {
            R.id.answer1 -> 0
            R.id.answer2 -> 1
            R.id.answer3 -> 2
            R.id.answer4 -> 3
            else -> -1
        }

        if (selectedAnswerIndex == questions[currentQuestionIndex].correctAnswer) {
            correctAnswersCount++
            totalPoints += questions[currentQuestionIndex].points
        }
    }

    private fun showResult(correctAnswers: Int, totalQuestions: Int) {
        val fragment = ResultFragment.newInstance(correctAnswers, totalQuestions)
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, fragment)
            .addToBackStack(null)
            .commit()
    }

    private fun saveUserScore(totalPoints: Int) {
        val user = auth.currentUser
        user?.let {
            val db = FirebaseFirestore.getInstance()
            val userRef = db.collection("highscores").document(user.email!!) // Koristimo email kao userId

            userRef.get()
                .addOnSuccessListener { document ->
                    if (document.exists()) {
                        val currentPoints = document.getLong("points") ?: 0
                        val newPoints = currentPoints + totalPoints

                        userRef.update("points", newPoints)
                            .addOnSuccessListener {
                                Toast.makeText(this, "Score updated successfully!", Toast.LENGTH_SHORT).show()
                            }
                            .addOnFailureListener { e ->
                                Toast.makeText(this, "Error updating score: ${e.message}", Toast.LENGTH_SHORT).show()
                            }
                    } else {
                        val userScore = hashMapOf(
                            "userId" to user.email,
                            "points" to totalPoints
                        )

                        userRef.set(userScore)
                            .addOnSuccessListener {
                                Toast.makeText(this, "Score saved successfully!", Toast.LENGTH_SHORT).show()
                            }
                            .addOnFailureListener { e ->
                                Toast.makeText(this, "Error saving score: ${e.message}", Toast.LENGTH_SHORT).show()
                            }
                    }
                }
                .addOnFailureListener { e ->
                    Toast.makeText(this, "Error fetching current score: ${e.message}", Toast.LENGTH_SHORT).show()
                }
        }
    }

}

data class Question(
    val text: String = "",
    val answers: List<String> = listOf(),
    val correctAnswer: Int = 0,
    val points: Int = 0,
)
