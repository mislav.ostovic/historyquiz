package com.mislavostovic.historyquiz

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment

class ResultFragment : Fragment() {

    private var correctAnswers: Int = 0
    private var totalQuestions: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_result, container, false)

        correctAnswers = arguments?.getInt("correct_answers") ?: 0
        totalQuestions = arguments?.getInt("total_questions") ?: 0

        val resultText: TextView = view.findViewById(R.id.result_text)
        val mainPageButton: Button = view.findViewById(R.id.main_page_button)

        resultText.text = "You got $correctAnswers out of $totalQuestions correct!"

        mainPageButton.setOnClickListener {
            val intent = Intent(requireActivity(), MainActivity::class.java)
            startActivity(intent)
            requireActivity().finish()
        }

        return view
    }

    companion object {
        @JvmStatic
        fun newInstance(correctAnswers: Int, totalQuestions: Int) =
            ResultFragment().apply {
                arguments = Bundle().apply {
                    putInt("correct_answers", correctAnswers)
                    putInt("total_questions", totalQuestions)
                }
            }
    }
}
